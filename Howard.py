#!/usr/bin/python3

import os
import time
import datetime
import requests
from Queue import Queue
from AccessToken import accessToken, chatID
from hosts import *
howardHostsPath = "/tmp/host_uptime/"

# Create the empty dictionary to store host history
hosts_history = {}

# Method:
# https://api.telegram.org/bot$accessToken/sendMessage?chatID=$chatID&text=Hello+World!


# Gameplan:
# Start a thread for each host (What do we do if the host list exceeds the
#                               available environment?)
# "tail -f" log file on server, per thread
# Store timestamp in variable for host (Do we want to store this variable on
#                                disk for restarting program/rebooting server?)
#                                         ^ Obviously
# All threads dump messages into a queue
# The queue is processed by the send_message function, and the lines are sent
# via Telegram, with a delay.

# We'll establish a queue for the messages
# https://docs.python.org/2/library/queue.html
# Set maxsize to something reasonable, may change in future
messageQueue = Queue(maxsize=65536)

# Establish a dictionary to store the last time stamp for each host
hostTimeStamp = {}


def send_message(botMessage, host):
    url_1 = "https://api.telegram.org/bot"
    url_2 = "/sendMessage?chat_id="
    url_3 = "&parse_mode=Markdown&text="
    sendURL = url_1 + accessToken + url_2 + chatID + url_3 + botMessage
    response = requests.get(sendURL)
    return response.json()
    print(response.json())
    # Rate limit messages sent by bot for sanity purposes.
    time.sleep(5)


def noise_filter():
    # Here we will filter out log noise.
    print("Noise Filter")

def host_check(hostType, host):
    if hostType == "pfSense":
        print("pfSense")
        # pfSense 2.5 and above will drop clog support
        #ssh_command = "ssh root@" + i + "clog /var/log/system.log"
        # for 2.5 and above
        #ssh_command = "ssh root@" + i + "cat /var/log/system.log"
        #pfLogs = os.system()
        #pfLogList = [line.strip() for line in pfLogs]
    elif hostType == "FreeNAS":
        # Do FreeNAS stuff here
        print("FreeNAS")
    else:
        # Ping the hosts
        print("Ping")

# Ping the hosts
def ping_hosts():
    for i in howardHosts:
        if not os.path.exists(howardHostsPath + i):
            os.mknod(howardHostsPath + i)
        if not os.path.exists(howardHostsPath + i + ".messaged"):
            os.mknod(howardHostsPath + i + ".messaged")
        response = os.system("ping -c 1 " + i)
        if response == 0:
            # Set the modification time
            # https://nitratine.net/blog/post/change-file-modification-time-in-python/
            os.utime(howardHostsPath + i, times=None)
            print("\nWe pinged host: " + i + "\n")
        else:
            print("\nWe pinged host: " + i)
            print("AND FAILED!\n")


# Test if the last ping is too old
def test_age():
    for i in howardHosts:
        downtime = int(
                    time.time()) - int(
                        os.path.getmtime(
                            howardHostsPath + i))

        print("downtime test on: " + i + " was: " + str(downtime))

        downtimeHumanReadable = time.strftime(
            "%a, %d %b %Y %H:%M:%S %Z",
            time.localtime(
                os.path.getmtime(
                    howardHostsPath + i)))

        if downtime >= 86400:
            # Test if we've sent a message lately, if so then rate-limit:
            messageTime = int(
                time.time()) - int(
                    os.path.getmtime(
                        howardHostsPath + i + ".messaged"))
            print(
                "We last sent a message about "
                + i + " on: " + str(messageTime))

            messageTimeHumanReadable = time.strftime(
                "%a, %d %b %Y %H:%M:%S %Z",
                time.localtime(
                    os.path.getmtime(
                        howardHostsPath + i + ".messaged")))

            if messageTime <= 86400:
                print(
                    "Not sending message: rate limited\nLast message time: "
                    + messageTimeHumanReadable)
            else:
                # Send message
                send_message(
                    "We ran into an error with host: "
                    + i + "\nHost has been down since: "
                    + downtimeHumanReadable, i)
                print("We get signal")
                # https://stackoverflow.com/questions/237079/how-to-get-file-creation-modification-date-times-in-python


def wellness_check():
    print("Wellness check was called")
    if datetime.datetime.today().weekday() == 6 and datetime.datetime.now().hour == 16:
        print("Sent Wellness Check " + "Day: " + today + "Hour: " + hour)
        send_message(
            "Hi!  I'm still running, I just haven't had anything to report",
            " :)")


while True:
    ping_hosts()
    test_age()
    wellness_check()
    print("Waiting 1 hour for next check")
    time.sleep(3600)

# FreeNAS logs: ssh root@midgard.home cat /var/log/messages
# pfSense logs: ssh root@heimdall.home clog /var/log/system.log

# do ping of simple hosts
# pull logs from pf hosts
# pull logs from FN hosts
